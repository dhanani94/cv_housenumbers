import logging

import numpy as np
import tensorflow as tf
from nn_catalog.neural_network_keras import NeuralNetworkKeras

logger = logging.getLogger(__name__)


class FullyConnected(NeuralNetworkKeras):
    def __init__(self):
        super().__init__()
        self.num_classes = None
        self.optimiser = 'adam'
        self.loss_func = 'sparse_categorical_crossentropy'
        self.metrics = ['accuracy']

    def build_model(self, images):
        # copied directly from: https://pythonprogramming.net/introduction-deep-learning-python-tensorflow-keras/
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu, input_shape=images.shape[1:]))
        model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
        model.add(tf.keras.layers.Dense(self.num_classes, activation=tf.nn.softmax))
        return model

    def fit(self, x_train, y_train, epochs=3):
        self.num_classes = len(y_train)
        self.model = self.build_model(x_train)
        self.model.compile(optimizer=self.optimiser,
                           loss=self.loss_func,
                           metrics=self.metrics)
        self.model.fit(x_train, y_train, epochs=epochs)
