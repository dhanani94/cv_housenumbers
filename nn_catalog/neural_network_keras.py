import logging

import tensorflow as tf

logger = logging.getLogger(__name__)


class NeuralNetworkKeras(object):
    def __init__(self):
        self.model = None
        self.loading_model_path = None

    def evaluate(self, x_test, y_test):
        val_loss, val_acc = self.model.evaluate(x_test, y_test)
        return val_loss, val_acc

    def predict(self, x):
        return self.model.predict(x)

    def save_model(self, model_file):
        self.model.save(model_file)

    def load_model(self):
        if not self.loading_model_path:
            raise Exception("Model path not correctly set. Please Check 'loading_model_path' value in config")
        self.model = tf.keras.models.load_model(self.loading_model_path)
