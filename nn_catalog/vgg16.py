import logging
import os

import numpy as np
import tensorflow as tf
from sklearn.utils import shuffle

from nn_catalog.neural_network_keras import NeuralNetworkKeras

logger = logging.getLogger(__name__)


class VGG16(NeuralNetworkKeras):
    def __init__(self, config):
        super().__init__()
        self.name = config["model_type"]
        self.output_dir = config.get("model_output_dir", "./models")

        if not os.path.isdir(self.output_dir):
            os.makedirs(self.output_dir)

        self.val_split = config.get("validation_split")
        self.augmentation = config.get("augmentation")
        self.loading_model_path = config.get("model_path")
        self.imagenet_weights_file = config.get("imagenet_weights_file")
        self.log_dir_tensorboard = config.get("log_dir_tensorboard")
        self.print_summary = config.get("print_summary", True)
        self.epochs = config.get("epochs")
        self.saving_period = config.get("saving_period")
        self.save_best_only = config.get("save_best_only", True)
        self.batch_size = config.get("batch_size", 1)
        self.early_stop_patience = config.get("early_stop_patience")
        self.num_classes = None
        self.model = None
        if config.get("optimiser") == "adam":
            self.optimiser = tf.keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, decay=0.000001, amsgrad=False)
        else:
            self.optimiser = tf.keras.optimizers.SGD(lr=0.001, decay=0.000001, momentum=0.9, nesterov=True)
        self.loss_func = 'categorical_crossentropy'
        self.metrics = ['accuracy']

    def build_model(self, input_shape):
        activation = 'relu'
        input_shape = input_shape[1:]
        logger.info("input_shape: {}".format(input_shape))
        input_0 = tf.keras.layers.Input(shape=input_shape)

        block_1 = tf.keras.layers.Conv2D(64, (3, 3), padding='same', activation=activation,
                                         name='block1_conv1')(input_0)
        block_1 = tf.keras.layers.Conv2D(64, (3, 3), padding='same',
                                         name='block1_conv2', activation=activation)(block_1)
        pool_1 = tf.keras.layers.MaxPooling2D((2, 2))(block_1)

        block_2 = tf.keras.layers.Conv2D(128, (3, 3), padding='same',
                                         name='block2_conv1', activation=activation)(pool_1)
        block_2 = tf.keras.layers.Conv2D(128, (3, 3), padding='same',
                                         name='block2_conv2', activation=activation)(block_2)
        pool_2 = tf.keras.layers.MaxPooling2D((2, 2))(block_2)

        block_3 = tf.keras.layers.Conv2D(256, (3, 3), padding='same',
                                         name='block3_conv1', activation=activation)(pool_2)
        block_3 = tf.keras.layers.Conv2D(256, (3, 3), padding='same',
                                         name='block3_conv2', activation=activation)(block_3)
        block_3 = tf.keras.layers.Conv2D(256, (3, 3), padding='same',
                                         name='block3_conv3', activation=activation)(block_3)
        pool_3 = tf.keras.layers.MaxPooling2D((2, 2))(block_3)

        block_4 = tf.keras.layers.Conv2D(512, (3, 3), padding='same',
                                         name='block4_conv1', activation=activation)(pool_3)
        block_4 = tf.keras.layers.Conv2D(512, (3, 3), padding='same',
                                         name='block4_conv2', activation=activation)(block_4)
        block_4 = tf.keras.layers.Conv2D(512, (3, 3), padding='same',
                                         name='block4_conv3', activation=activation)(block_4)
        pool_4 = tf.keras.layers.MaxPooling2D((2, 2))(block_4)

        block_5 = tf.keras.layers.Conv2D(512, (3, 3), padding='same',
                                         name='block5_conv1', activation=activation)(pool_4)
        block_5 = tf.keras.layers.Conv2D(512, (3, 3), padding='same',
                                         name='block5_conv2', activation=activation)(block_5)
        block_5 = tf.keras.layers.Conv2D(512, (3, 3), padding='same',
                                         name='block5_conv3', activation=activation)(block_5)
        pool_5 = tf.keras.layers.MaxPooling2D((2, 2))(block_5)

        block_6 = tf.keras.layers.Flatten()(pool_5)
        block_6 = tf.keras.layers.Dense(4096, activation='relu')(block_6)
        block_6 = tf.keras.layers.Dense(4096, activation='relu')(block_6)
        block_6 = tf.keras.layers.Dense(1000, activation='relu')(block_6)
        block_6 = tf.keras.layers.Dense(self.num_classes, activation=tf.nn.softmax)(block_6)

        model = tf.keras.models.Model(input_0, block_6)
        if self.print_summary:
            logger.info(model.summary())

        if self.imagenet_weights_file:
            logger.info("loading imagenet weights")
            # weights_list = model.get_weights()
            # for i, weights in enumerate(weights_list[1:14]):
            #     model.layers[i].set_weights(weights)
            model.load_weights(self.imagenet_weights_file, by_name=True)
        return model

    def fit(self, x_train, y_train):
        self.num_classes = y_train.shape[1]
        # self.num_classes = len(np.unique(y_train))
        if not self.model:
            self.model = self.build_model(x_train.shape)
            self.model.compile(optimizer=self.optimiser, loss=self.loss_func, metrics=self.metrics)
        # https://www.tensorflow.org/api_docs/python/tf/keras/callbacks/ModelCheckpoint
        # https://www.tensorflow.org/api_docs/python/tf/keras/callbacks/TensorBoard
        # https://www.tensorflow.org/api_docs/python/tf/keras/callbacks/EarlyStopping
        callbacks = []

        if self.early_stop_patience is not None:
            logger.info("creating callback for EarlyStopping : {}".format(self.log_dir_tensorboard))
            callbacks.append(tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=self.early_stop_patience,
                                                              verbose=1))
        if self.saving_period:
            logger.info("creating callback for saving model. Best only mode: {}".format(self.save_best_only))
            model_file_base = "{}/{}_".format(self.output_dir, self.name)
            model_file = model_file_base + "{epoch:02d}-{val_acc:.2f}.model"
            callbacks.append(tf.keras.callbacks.ModelCheckpoint(model_file, monitor='val_acc', verbose=1,
                                                                save_best_only=self.save_best_only))
        if self.log_dir_tensorboard:
            logger.info("creating callback for tensorboard logs at: {}".format(self.log_dir_tensorboard))
            callbacks.append(tf.keras.callbacks.TensorBoard(log_dir=self.log_dir_tensorboard, write_images=True))
        else:
            callbacks = None
        logger.info("setting batch size to {}, for {} epochs".format(self.batch_size, self.epochs))
        if self.augmentation:
            logger.info("augmenting the images before training!")
            # https://keras.io/preprocessing/image/
            # possibly play with zca whitening
            datagen = tf.keras.preprocessing.image.ImageDataGenerator(rotation_range=20, width_shift_range=0.2,
                                                                      height_shift_range=0.2, shear_range=0.5,
                                                                      zoom_range=0.5, channel_shift_range=0.5)

            # compute quantities required for featurewise normalization
            # (std, mean, and principal components if ZCA whitening is applied)
            datagen.fit(x_train)
            logger.info("generating the validation data")
            x_train, y_train = shuffle(x_train, y_train)
            num_val = int(x_train.shape[0] * self.val_split)

            x_val = x_train[:num_val]
            x_train = x_train[num_val:]

            y_val = y_train[:num_val]
            y_train = y_train[num_val:]

            # fits the model on batches with real-time data augmentation:
            self.model.fit_generator(datagen.flow(x_train, y_train, batch_size=self.batch_size),
                                     validation_data=(x_val, y_val), steps_per_epoch=len(x_train) / 32,
                                     epochs=self.epochs, callbacks=callbacks)
        else:
            self.model.fit(x_train, y_train, batch_size=self.batch_size, epochs=self.epochs,
                           validation_split=self.val_split, callbacks=callbacks)
