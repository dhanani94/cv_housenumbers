import logging
import os

import cv2
import numpy as np

logger = logging.getLogger(__name__)


# file designed to download the image set and properly store them locally
def read_images_from_dir_np(directory, out_file=None, limit=None, img_size=32):
    logger.info("reading images from: {}".format(directory))
    all_img_files = [name for name in os.listdir(directory) if os.path.splitext(name)[-1].lower() in [".jpg", ".png"]]
    if limit:
        all_img_files = all_img_files[:limit]

    total_files = len(all_img_files)
    logger.info("creating output data structure")
    output = np.ndarray((total_files, img_size, img_size, 3))
    counter = 0
    for filename in all_img_files:
        logger.info("reading: {} ({}/{})".format(filename, counter + 1, total_files))
        file_path = os.path.join(directory, filename)
        image = cv2.imread(file_path)
        image = cv2.resize(image, (img_size, img_size))
        output[counter, ...] = image[:, :, :]
        counter += 1

    logger.info("output array shape: {}".format(output.shape))
    if out_file:
        np.save(out_file, output)
    return output


def video_frame_generator(filename):
    # Open file with VideoCapture and set result to 'video'. Replace None
    video = cv2.VideoCapture(filename)
    # Do not edit this while loop
    while video.isOpened():
        ret, frame = video.read()
        if ret:
            yield frame
        else:
            break
    video.release()
    yield None


def mp4_video_writer(filename, frame_size, fps=20):
    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    return cv2.VideoWriter(filename, fourcc, fps, frame_size)


class ImageProcessor(object):
    def __init__(self, config):
        self.valid_image_types = [".jpg", ".png"]
        self.target_dir = config.get("target_dir")
        self.patch_size = config.get("patch_size")
        self.step_size = config.get("step_size")
        self.save_image = config.get("save_image", False)
        self.show_image = config.get("show_image", False)
        self.max_dims = config.get("max_dims", 400)
        self.output_dir = config.get("output_dir", None)
        self.dist_thresh = config.get("dist_thresh", 20)
        self.score_threshold = config.get("score_threshold", None)
        self.in_video_file = config.get("in_video_file", None)
        self.out_video_name = config.get("out_video_name", "prediction_video.mp4")
        if not os.path.isdir(self.output_dir):
            logger.info("creating directory: {}".format(self.output_dir))
            os.makedirs(self.output_dir)
        self.fps = 30

    def run_on_video(self, function):
        image_gen = video_frame_generator(self.in_video_file)
        image = image_gen.__next__()
        if self.max_dims and image.shape[0] > self.max_dims or image.shape[1] > self.max_dims:
            image = self.resize_to_max_dim(image, self.max_dims)
        h, w, d = image.shape
        out_path = "{}/{}".format(self.output_dir, self.out_video_name)
        video_out = mp4_video_writer(out_path, (w, h), self.fps)
        frame_num = 1
        while image is not None:
            logger.info("Processing fame {}".format(frame_num))
            if self.max_dims and image.shape[0] > self.max_dims or image.shape[1] > self.max_dims:
                image = self.resize_to_max_dim(image, self.max_dims)
            best_guess = self.apply_fn_to_sliding_window(image, function)
            image, pred_text = draw_pred_on_images_and_get_text(best_guess, image)

            if frame_num < 20:
                filename = "{}/image_{}.png".format(self.output_dir, frame_num)
                cv2.imwrite(filename, image)

            video_out.write(image)
            image = image_gen.__next__()
            frame_num += 1
        video_out.release()

    def run_on_all_images(self, images, function):
        for i, image in enumerate(images):
            draw_image = None
            pred_text = None
            best_guess = self.apply_fn_to_sliding_window(image, function)
            if self.save_image:
                if not self.output_dir:
                    raise Exception("You Need to specify output_dir in config")

                draw_image, pred_text = draw_pred_on_images_and_get_text(best_guess, image)
                filename = "{}/{}.png".format(self.output_dir, i + 1)
                cv2.imwrite(filename, draw_image)
            if self.show_image:
                if not draw_image:
                    draw_image, pred_text = draw_pred_on_images_and_get_text(best_guess, image)
                cv2.imshow(pred_text, draw_image)
                cv2.waitKey()
                cv2.destroyAllWindows()

    def read_images_from_dir(self):
        if not self.target_dir:
            raise Exception("ImageProcessor target_dir is not set. Please check configuration file!")
        logger.info("reading images from: {}".format(self.target_dir))
        images_list = []
        for filename in os.listdir(self.target_dir):
            ext = os.path.splitext(filename)[-1]
            if ext.lower() in self.valid_image_types:
                logger.info("reading: {}".format(filename))
                file_path = os.path.join(self.target_dir, filename)
                image = cv2.imread(file_path)
                if self.max_dims and image.shape[0] > self.max_dims or image.shape[1] > self.max_dims:
                    image = self.resize_to_max_dim(image, self.max_dims)
                images_list.append(image)
        logger.info("found {} images".format(len(images_list)))
        return images_list

    def get_image_pyramid(self, image):
        logger.info("calculating pyramid")
        pyramid = [image]
        counter = 0
        while counter < 6:
            lower_reso = cv2.pyrDown(pyramid[counter])
            counter += 1
            if lower_reso.shape[0] < self.patch_size or lower_reso.shape[1] < self.patch_size:
                break
            logger.info("loweres shape: {}".format(lower_reso.shape))
            pyramid.append(lower_reso)
        logger.info("Generated {} pyramid images".format(len(pyramid)))
        return pyramid

    def apply_fn_to_sliding_window(self, input_image, function):
        # https://www.pyimagesearch.com/2015/03/23/sliding-windows-for-object-detection-with-python-and-opencv/

        logger.info("creating patches for image")
        pyramid = self.get_image_pyramid(input_image)

        nms_input = {}  # inputs for the non-max-supression
        pred_score_box_list = []
        total_patches_found = 0
        removed_by_threshold = 0
        for lvl, image in enumerate(pyramid):
            scale_factor = 2 ** lvl
            patches = []
            patches_metadata = []
            counter = 0
            for y in range(0, image.shape[0], self.step_size):  # go through each row
                for x in range(0, image.shape[1], self.step_size):  # go through each column
                    dims = [y, x, y + self.patch_size, x + self.patch_size]
                    if dims[3] > image.shape[1] or dims[2] > image.shape[0]:
                        continue
                    patches.append(image[dims[0]:dims[2], dims[1]:dims[3]])
                    patches_metadata.append(dims)
                    counter += 1
            patches = np.array(patches)
            logger.info(
                "processed {} patches of shape: {} and scale factor of {}".format(counter, patches.shape, scale_factor))
            fn_val = function(patches)  # run the algorithm!!!
            argmax_array = np.argmax(fn_val, axis=1)  # prediction
            max_array = np.max(fn_val, axis=1)  # confidence
            total_patches_found += len(argmax_array)
            for i, pred in enumerate(argmax_array):  # iterate through all predictions
                if pred == 10:
                    continue
                conf = max_array[i]
                if self.score_threshold and conf < self.score_threshold:
                    removed_by_threshold += 1
                    continue

                dims_scaled = np.multiply(patches_metadata[i], scale_factor).astype(float)
                if pred in nms_input.keys():
                    nms_input[pred].append((conf, dims_scaled))
                else:
                    nms_input[pred] = [(conf, dims_scaled)]

        logger.info("{} possible number segments".format(total_patches_found))
        remaining_patches = total_patches_found - removed_by_threshold
        logger.info("{} with scores higher than {}".format(remaining_patches, self.score_threshold))
        pred_score_box_list.extend(non_max_suppress(nms_input))
        # draw_pred_on_images(pred_score_box_list, input_image)
        return choose_best_guess(pred_score_box_list, dist_tresh=self.dist_thresh)

    def resize_to_max_dim(self, input_image, max_dim):
        aspect_ratio = input_image.shape[0] / input_image.shape[1]
        if aspect_ratio < 1:
            height = int(max_dim * aspect_ratio)
            width = int(max_dim)
        else:
            width = int(max_dim / aspect_ratio)
            height = int(max_dim)
        logger.info(
            "resize_img ({},{}) to ({},{})".format(input_image.shape[1], input_image.shape[0], height, width))
        input_image = cv2.resize(input_image, (width, height))
        return input_image


def choose_best_guess(pred_score_box_list, dist_tresh=20, num_consider=6):
    logger.info("sorting pred_score_box_list")
    pred_score_box_list = sorted(pred_score_box_list, key=lambda x: x[1], reverse=True)
    if num_consider and len(pred_score_box_list) > num_consider:
        pred_score_box_list = pred_score_box_list[:num_consider]
    pred_to_psbc_dict = {}

    logger.info("calculating centers and grouping by prediction")
    for item in pred_score_box_list:
        pred = item[0]
        center = (np.mean([item[2][3], item[2][1]]), np.mean([item[2][2], item[2][0]]))
        pred_score_box_center = (pred, item[1], item[2], center)
        if pred in pred_to_psbc_dict.keys():
            pred_to_psbc_dict[pred].append(pred_score_box_center)
        else:
            pred_to_psbc_dict[pred] = [pred_score_box_center]

    all_verified_p_s_b_c = []
    for pred_key in pred_to_psbc_dict.keys():
        single_p_s_b_c_list = pred_to_psbc_dict[pred_key]
        i_num = len(single_p_s_b_c_list)
        verified_p_s_b_c = []
        while single_p_s_b_c_list:
            cent_a = single_p_s_b_c_list.pop(0)
            verified_p_s_b_c.append(cent_a)
            for cent_b in single_p_s_b_c_list:
                dist = np.sqrt(np.square(cent_a[3][0] - cent_b[3][0]) + np.square(cent_a[3][1] - cent_b[3][1]))
                if dist < dist_tresh:
                    logger.info("dist: {}. Removing prediction: {}-{}".format(dist, cent_b[1], cent_b[2]))
                    single_p_s_b_c_list.remove(cent_b)

        logger.info("pred {} now has centers: {} (previously {})".format(pred_key, len(verified_p_s_b_c), i_num))
        all_verified_p_s_b_c.extend(verified_p_s_b_c)
    # pred_score_box_list = [(p[0], p[1], p[2]) for p in all_verified_p_s_b_c]
    # return pred_score_box_list
    logger.info("all_verified_p_s_b_c: {}".format(all_verified_p_s_b_c))
    return all_verified_p_s_b_c


def write_text_on_image(text, image, x, y):
    DEFAULT_FONT = cv2.FONT_HERSHEY_SIMPLEX
    # http://answers.opencv.org/question/203/is-it-possible-to-measure-in-pixels-a-character-outputed-from-puttext/
    # new_color = ps2.get_random_color()
    size = cv2.getTextSize(text, DEFAULT_FONT, 0.5, 1)
    text_width = int(size[0][0])
    text_height = int(size[0][1])
    rect_p1 = (x + text_width, y - text_height)
    rect_p2 = (x, y + text_height)
    cv2.rectangle(image, rect_p1, rect_p2, (255, 255, 255), -1)
    cv2.putText(image, text, (x, y), DEFAULT_FONT, 0.5, (0, 0, 0), 2, cv2.LINE_AA)
    return image


def save_pred_images_and_text(p_s_b_c_list, image, output_image_name, output_file_name):
    logger.info(p_s_b_c_list)
    logger.info("sorting pred_score_box_list")
    p_s_b_c_list = sorted(p_s_b_c_list, key=lambda x: x[1], reverse=True)

    draw_img = image.copy()
    # logger.info(pred_score_box_list[:5])
    logger.info("drawing predictions on image")
    for pred, score, box, center in p_s_b_c_list:
        y1, x1, y2, x2 = [int(p) for p in box]
        center_x, center_y = [int(p) for p in center]
        cv2.rectangle(draw_img, (x1, y1), (x2, y2), (255, 0, 0))
        cv2.circle(draw_img, (center_x, center_y), 4, (0, 255, 0))

    x_cods = [p[3][0] for p in p_s_b_c_list]
    y_cods = [p[3][1] for p in p_s_b_c_list]
    if np.var(y_cods) > np.var(x_cods):
        logger.info("this is vertically aligned")
        p_s_b_c_sorted_axis = sorted(p_s_b_c_list, key=lambda x: x[3][1])
    else:
        logger.info("this probably horizontally aligned")
        p_s_b_c_sorted_axis = sorted(p_s_b_c_list, key=lambda x: x[3][0])
    probably_nums = [p[0] for p in p_s_b_c_sorted_axis]
    logger.info("I THINK THIS IS: {}".format(probably_nums))
    text = "pred: {}".format("".join(probably_nums))
    draw_img = write_text_on_image(text, draw_img, 0, 0)
    cv2.imshow("pred: {}".format(probably_nums), draw_img)
    cv2.waitKey()
    cv2.destroyAllWindows()


def draw_pred_on_images_and_get_text(p_s_b_c_list, image):
    logger.info(p_s_b_c_list)
    logger.info("sorting pred_score_box_list")
    p_s_b_c_list = sorted(p_s_b_c_list, key=lambda x: x[1], reverse=True)

    draw_img = image.copy()
    # logger.info(pred_score_box_list[:5])
    logger.info("drawing predictions on image")
    for pred, score, box, center in p_s_b_c_list:
        y1, x1, y2, x2 = [int(p) for p in box]
        center_x, center_y = [int(p) for p in center]
        cv2.rectangle(draw_img, (x1, y1), (x2, y2), (255, 0, 0))
        cv2.circle(draw_img, (center_x, center_y), 4, (0, 255, 0))

    x_cods = [p[3][0] for p in p_s_b_c_list]
    y_cods = [p[3][1] for p in p_s_b_c_list]
    if np.var(y_cods) > np.var(x_cods):
        logger.info("this is vertically aligned")
        p_s_b_c_sorted_axis = sorted(p_s_b_c_list, key=lambda x: x[3][1])
    else:
        logger.info("this probably horizontally aligned")
        p_s_b_c_sorted_axis = sorted(p_s_b_c_list, key=lambda x: x[3][0])
    probably_nums = [str(p[0]) for p in p_s_b_c_sorted_axis]
    logger.info("I THINK THIS IS: {}".format(probably_nums))
    text = "pred: {}".format("".join(probably_nums))
    return write_text_on_image(text, draw_img, 0, 100), text


# https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/single-shot-detectors/yolo.html
def non_max_suppress(nms_input):
    """
    :param nms_input: dictionary that contains {label_#: { scores : [s], boxes  : [[y1, x1, y2, x2]] }, }
    :return:
    """
    output_pred_score_box_list = []
    for pred_class, v in nms_input.items():
        sorted_inputs = sorted(v, key=lambda x: x[0], reverse=True)
        iou_threshold = 0.25
        num_boxes = len(sorted_inputs)
        for i in range(num_boxes):  # dims are now going to be [y1, x1, y2, x2]
            score_a, box_a = sorted_inputs[i]
            if score_a == 0:
                continue
            output_pred_score_box_list.append((pred_class, score_a, box_a))
            for j in range(i + 1, num_boxes):
                score_b, box_b = sorted_inputs[j]
                if score_b == 0:
                    continue
                iou = get_iou(box_a, box_b)
                if iou >= iou_threshold:
                    sorted_inputs[j] = (0, [])  # suppress the highly overlapped boxed
    logger.info("NMS filtered to: {} boxes".format(len(output_pred_score_box_list)))
    return output_pred_score_box_list


# https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/single-shot-detectors/yolo.html
def get_iou(box_a, box_b):
    a_y1, a_x1, a_y2, a_x2 = box_a
    b_y1, b_x1, b_y2, b_x2 = box_b

    # overall bounding box
    x_left = max(a_x1, b_x1)
    y_top = max(a_y1, b_y1)
    x_right = min(a_x2, b_x2)
    y_bottom = min(a_y2, b_y2)

    if x_right < x_left or y_bottom < y_top:  # no overlap
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (a_x2 - a_x1) * (a_y2 - a_y1)
    bb2_area = (b_x2 - b_x1) * (b_y2 - b_y1)

    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    return iou
