import os
import os
import time
import logging
import os

import cv2
import numpy as np
import cv2

logger = logging.getLogger(__name__)


class VideoProcessor(object):
    def __init__(self, config):
        self.valid_image_types = [".jpg", ".png"]
        self.target_dir = config.get("target_dir")
        self.patch_size = config.get("patch_size")
        self.step_size = config.get("step_size")
        self.save_image = config.get("save_image", False)
        self.show_image = config.get("show_image", False)
        self.max_dims = config.get("max_dims", 400)
        self.output_dir = config.get("output_dir", None)
        self.score_threshold = config.get("score_threshold", None)
        if not os.path.isdir(self.output_dir):
            logger.info("creating directory: {}".format(self.output_dir))
            os.makedirs(self.output_dir)


def video_frame_generator(filename):
    # Open file with VideoCapture and set result to 'video'. Replace None
    video = cv2.VideoCapture(filename)
     # Do not edit this while loop
    while video.isOpened():
        ret, frame = video.read()
        if ret:
            yield frame
        else:
            break
    video.release()
    yield None


def mp4_video_writer(filename, frame_size, fps=20):
    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    return cv2.VideoWriter(filename, fourcc, fps, frame_size)


def read_video_file_from_dir(video_name, fps, frame_ids, output_prefix,
                             counter_init, is_part5, verbose=False):
    video = os.path.join(VID_DIR, video_name)
    image_gen = video_frame_generator(video)

    image = image_gen.__next__()
    h, w, d = image.shape

    out_path = "output/ar_{}-{}".format(output_prefix[4:], video_name)
    video_out = mp4_video_writer(out_path, (w, h), fps)

    # Optional template image
    template = cv2.imread(os.path.join(IMG_DIR, "template.jpg"))

    if is_part5:
        advert = cv2.imread(os.path.join(IMG_DIR, "img-3-a-1.png"))
        src_points = ps3.get_corners_list(advert)

    output_counter = counter_init

    frame_num = 1

    while image is not None:
        if verbose:
            print("Processing fame {}".format(frame_num))

        markers = ps3.find_markers(image, template)

        if is_part5:
            homography = ps3.find_four_point_transform(src_points, markers)
            image = ps3.project_imageA_onto_imageB(advert, image, homography)

        else:

            for marker in markers:
                mark_location(image, marker)

        frame_id = frame_ids[(output_counter - 1) % 3]

        if frame_num == frame_id:
            out_str = output_prefix + "-{}.png".format(output_counter)
            save_image(out_str, image)
            output_counter += 1

        video_out.write(image)

        image = image_gen.__next__()

        frame_num += 1

    video_out.release()
