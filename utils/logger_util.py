import logging
import os
from logging.handlers import RotatingFileHandler

logger = logging.getLogger(__name__)


def initialise_logger(log_level="info", output_dir="logs"):
    console_log_level = logging.INFO

    if log_level == "debug":
        console_log_level = logging.DEBUG

    date_fmt = '%Y-%m-%d %H:%M:%S'
    p_format = '%(asctime)s.%(msecs)03d %(name)-12s: %(levelname)s: %(message)s'

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    output_file = "{}/log.txt".format(output_dir)

    console_handler = logging.StreamHandler()
    logging.basicConfig(level=console_log_level, format=p_format, datefmt=date_fmt, handlers=[console_handler])
    main_logger = logging.getLogger('')

    # Add file handler to the root main_logger
    file_handler = RotatingFileHandler(output_file, maxBytes=50000000, backupCount=5)
    file_log_fmt = logging.Formatter(fmt=p_format, datefmt=date_fmt)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(file_log_fmt)
    main_logger.addHandler(file_handler)
