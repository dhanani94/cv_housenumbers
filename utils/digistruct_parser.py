# !/usr/bin/python
# Author: Priji prijip (https://github.com/prijip)
# License: not found
# https://github.com/prijip/Py-Gsvhn-DigitStruct-Reader


# Ref:https://confluence.slac.stanford.edu/display/PSDM/How+to+access+HDF5+data+from+Python

import h5py
import logging
import pickle

logger = logging.getLogger(__name__)


def read_digit_struct_group(ds_file):
    ds_group = ds_file["digitStruct"]
    return ds_group


def read_string(str_ref, ds_file):
    str_obj = ds_file[str_ref]
    full_str = ''.join(chr(i) for i in str_obj)
    return full_str


def read_int(int_array, ds_file):
    int_ref = int_array[0]
    is_reference = isinstance(int_ref, h5py.Reference)
    if is_reference:
        int_obj = ds_file[int_ref]
        int_val = int(int_obj[0])
    else:  # Assuming value type
        int_val = int(int_ref)
    return int_val


def yield_next_int(int_dataset, ds_file):
    for intData in int_dataset:
        int_val = read_int(intData, ds_file)
        yield int_val


def yield_next_b_box(bbox_dataset, ds_file):
    for bboxArray in bbox_dataset:
        bbox_group_ref = bboxArray[0]
        bbox_group = ds_file[bbox_group_ref]
        label_dataset = bbox_group["label"]
        left_dataset = bbox_group["left"]
        top_dataset = bbox_group["top"]
        width_dataset = bbox_group["width"]
        height_dataset = bbox_group["height"]

        left = yield_next_int(left_dataset, ds_file)
        top = yield_next_int(top_dataset, ds_file)
        width = yield_next_int(width_dataset, ds_file)
        height = yield_next_int(height_dataset, ds_file)

        bbox_list = []

        for label in yield_next_int(label_dataset, ds_file):
            x1 = next(left)
            x2 = x1 + next(width)
            y1 = next(top)
            y2 = y1 + next(height)
            bbox_list.append({"x1": x1, "x2": x2, "y1": y1, "y2": y2, "label": label})

        yield bbox_list


def yield_next_file_name(name_dataset, ds_file):
    for name_array in name_dataset:
        name_ref = name_array[0]
        name = read_string(name_ref, ds_file)
        yield name


def yield_next_digit_struct(ds_file_name):
    ds_file = h5py.File(ds_file_name, 'r')
    ds_group = read_digit_struct_group(ds_file)
    name_dataset = ds_group["name"]
    bbox_dataset = ds_group["bbox"]
    bbox_list_iter = yield_next_b_box(bbox_dataset, ds_file)
    for name in yield_next_file_name(name_dataset, ds_file):
        yield {"name": name, "bbox_list": next(bbox_list_iter)}


def parse_digistruct_to_dict(ds_file_name, pickle_name=None, limit=None):
    output = []
    counter = 0
    logger.info("parsing digistruct file: {}".format(ds_file_name))
    for ds_obj in yield_next_digit_struct(ds_file_name):
        output.append(ds_obj)
        counter += 1
        if limit and counter >= limit:
            break
    logger.info("successfuly retrieved {} images data".format(len(output)))
    if pickle_name:
        logger.info("picking file: {} (will be replaced if already exists!)".format(pickle_name))
        f = open(pickle_name, "wb")
        pickle.dump(output, f)
        f.close()
        logger.info("successfully saved {}".format(pickle_name))
    return output
