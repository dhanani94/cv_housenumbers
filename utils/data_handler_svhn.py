import logging
import os
import pickle
import tarfile
import urllib.request

import cv2
import numpy as np
import tensorflow as tf
from scipy.io import loadmat

logger = logging.getLogger(__name__)


class DataHandlerSVHN(object):
    def __init__(self, config):
        self.size_limit = config.get("size_limit")
        self.data_dir = config["data_dir"]
        self.bg_img_file = config["bg_img_file"]
        self.bg_limit = config["bg_limit"]

        self.download_dir = "{}/download".format(self.data_dir)
        self.train_dir = "{}/train".format(self.data_dir)
        self.test_dir = "{}/test".format(self.data_dir)
        self.extra_dir = "{}/extra".format(self.data_dir)

    def download_url(self, url):
        # https://therenegadecoder.com/code/how-to-check-if-a-file-exists-in-python/
        filename = url.split("/")[-1]
        local_file = os.path.join(self.download_dir, filename)
        if os.path.isfile(local_file):
            logger.info("found file locally: {}".format(filename))
            return local_file
        else:
            logger.info("downloading: {} to {}".format(url, local_file))
            urllib.request.urlretrieve(url, local_file)
        return local_file

    def download_files(self):
        files = {}
        if not os.path.isdir(self.download_dir):
            os.makedirs(self.download_dir)
        files["train_32x32_file"] = self.download_url("http://ufldl.stanford.edu/housenumbers/train_32x32.mat")
        files["test_32x32_file"] = self.download_url("http://ufldl.stanford.edu/housenumbers/test_32x32.mat")
        return files

    def unzip_file(self, filename):
        logger.info("unzipping: {}".format(filename))
        tar = tarfile.open(filename, "r:gz")
        tar.extractall(self.data_dir, self.data_dir)
        tar.close()

    def process_mat_files(self, mat_file, dtype="float64"):
        # https://scipy-cookbook.readthedocs.io/items/Reading_mat_files.html
        # input is [in_height, in_width, in_channels, batch]
        # need data in form [batch, in_height, in_width, in_channels]
        # also data should be floats, and normalised i think
        logger.info("processing extracting data from mat file")
        data = loadmat(mat_file)
        labels = data['y'].flatten()
        images = data['X'].astype(dtype)

        logger.info("restructuring data")
        images = np.moveaxis(images, -1, 0)
        return images, labels

    def unzip_files(self, files):
        if not os.path.isdir(self.train_dir):
            self.unzip_file(files["train_file"])
        if not os.path.isdir(self.test_dir):
            self.unzip_file(files["test_file"])
        return "train", "test"

    def retrieve_train_test_data(self):
        logger.info("downloading all files of type: SVHN")
        files = self.download_files()
        logger.info("processing file: {}".format("train_32x32_file"))
        x_train, y_train = self.process_mat_files(files["train_32x32_file"], dtype="float32")
        logger.info("retrieved from dataset x_train: {}, y_train: {}".format(len(x_train), len(y_train)))

        logger.info("processing file: {}".format("test_32x32_file"))
        x_test, y_test = self.process_mat_files(files["test_32x32_file"], dtype="float32")
        logger.info("retrieved from dataset x_test: {}, y_test: {}".format(len(x_test), len(y_test)))

        logger.info("setting y values from 0-9 not 1-10 (0 becomes 0)")
        y_train = y_train % 10
        y_test = y_test % 10

        if self.size_limit:
            logger.info("limiting size to: {}".format(self.size_limit))
            logger.info("original x_train: {} and y_train: {}".format(len(x_train), len(y_train)))
            logger.info("original x_test: {} and y_test: {}".format(len(x_test), len(y_test)))
            test_train_ratio = len(y_test) / len(y_train)
            test_size_lim = int(test_train_ratio * self.size_limit)
            train_size_lim = self.size_limit - test_size_lim

            x_train = x_train[:train_size_lim]
            y_train = y_train[:train_size_lim]

            x_test = x_test[:test_size_lim]
            y_test = y_test[:test_size_lim]

            logger.info("resized x_train: {} and y_train: {}".format(len(x_train), len(y_train)))
            logger.info("resized x_test: {} and y_test: {}".format(len(x_test), len(y_test)))

        x_train, y_train, x_test, y_test = self.append_bg_to_train_test(x_train, y_train, x_test, y_test)

        logger.info("one hot encoding data categorically")
        y_train = tf.keras.utils.to_categorical(y_train)
        y_test = tf.keras.utils.to_categorical(y_test)
        return x_train, y_train, x_test, y_test

    def append_bg_to_train_test(self, x_train, y_train, x_test, y_test):
        bg_images = np.load(self.bg_img_file)
        # the bg images should be only like 15% of the test and training size
        len_train = len(y_train)
        len_test = len(y_test)
        len_bg = bg_images.shape[0]
        test_train_ratio = len_test / len_train
        logger.info("test_train_ratio:{}".format(test_train_ratio))

        len_bg_test = int(test_train_ratio * len_bg)
        len_bg_train = len_bg - len_bg_test

        logger.info("len_bg:{}".format(len_bg))
        logger.info("len_bg_test:{}".format(len_bg_test))
        logger.info("len_bg_train:{}".format(len_bg_train))

        if len_bg_test > self.bg_limit * len_test:
            len_bg_test = int(self.bg_limit * len_test)
            logger.info("reducing bg_test size to: {}".format(len_bg_test))

        if len_bg_train > self.bg_limit * len_train:
            len_bg_train = int(self.bg_limit * len_train)
            logger.info("reducing bg_train size to: {}".format(len_bg_train))

        logger.info("reading bg image file: {}".format(self.bg_img_file))
        bg_images = np.load(self.bg_img_file)

        logger.info("shuffling bg image array")
        np.random.shuffle(bg_images)

        logger.info("creating x_train, x_test, y_train, y_test for bg")
        bg_x_train = bg_images[:len_bg_train, ...]
        bg_x_test = bg_images[len_bg_train:len_bg_test + len_bg_train, ...]
        bg_y_train = np.full(len_bg_train, 10)
        bg_y_test = np.full(len_bg_test, 10)

        logger.info("concatenating bg with training and testing data")
        x_train = np.concatenate((x_train, bg_x_train))
        x_test = np.concatenate((x_test, bg_x_test))
        y_train = np.concatenate((y_train, bg_y_train))
        y_test = np.concatenate((y_test, bg_y_test))

        return x_train, y_train, x_test, y_test


def create_bg_images(input_pickle, src_dir, output_dir):
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    digit_struct_list = pickle.load(open(input_pickle, "rb"))
    for img_data in digit_struct_list:
        logger.info("processing image: {}".format(img_data["name"]))
        input_file = "{}/{}".format(src_dir, img_data["name"])
        image = cv2.imread(input_file)
        patch_sizes = [32, 64, 80, 100, 120]
        x_min = 9999999999
        x_max = 0.
        y_min = 9999999999
        y_max = 0.
        for bbox in img_data["bbox_list"]:
            y_min = min(y_min, bbox["y1"])
            y_max = max(y_max, bbox["y2"])
            x_min = min(x_min, bbox["x1"])
            x_max = max(x_max, bbox["x2"])
        for patch_size in patch_sizes:
            for y in range(0, image.shape[0], patch_size):  # go through each row
                for x in range(0, image.shape[1], patch_size):  # go through each column
                    x2 = x + patch_size
                    y2 = y + patch_size
                    if x2 > image.shape[1] or y2 > image.shape[0]:
                        logger.info("patch out of frame... skipping!")
                        continue
                    if (x_min > x2) or (x > x_max) or (y_min > y2) or (y > y_max):
                        out_file = "{}/{}".format(output_dir, "{}_{}_{}_{}".format(patch_size, x, y, img_data["name"]))
                        patch = image[y:y2, x:x2]
                        cv2.imwrite(out_file, patch)
                    else:
                        logger.info("there is intersection with a number, skipping patch")
                        pass
