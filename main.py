import argparse
import logging
import os

from nn_catalog.mini_vgg import MiniVGG
from nn_catalog.vgg16 import VGG16
from utils import logger_util
from utils.data_handler_svhn import DataHandlerSVHN
from utils.image_processor import ImageProcessor

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'  # not sure what this does
logger = logging.getLogger(__name__)

# https://stackoverflow.com/questions/46036522/defining-model-in-keras-include-top-true
# https://www.tensorflow.org/tutorials/keras/save_and_restore_models
# https://github.com/fchollet/deep-learning-models/releases/download/v0.1/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5

name_of_model_for_training = "vgg_16_scratch_aug_b128_es_vs10_adam"

training_config = {
    # "imagenet_weights_file": "./models/image_net_vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5",
    "validation_split": 0.10,
    "model_type": "VGG16",
    "model_output_dir": "./models/{}".format(name_of_model_for_training),
    "epochs": 25,
    "saving_period": 2,
    "save_best_only": True,
    "log_dir_tensorboard": "./models/{}".format(name_of_model_for_training),
    "batch_size": 128,
    "early_stop_patience": 5,
    "augmentation": True,
    "optimiser": "adam"
}

data_handler_config = {
    "data_set": "housenumbers",
    "size_limit": None,
    "data_dir": "./data",
    "bg_img_file": './data/bg_images.npy',
    "bg_limit": 0.15
}

predict_config = {
    "model_type": "MiniVGG16",
    # "model_path": "./models/vgg_scratch_aug_b70_es_vs10/VGG16_25-0.94.model",
    "model_path": "./models/vgg_image_net_aug_b128_es_vs10/VGG16_12-0.95.model",
    "target_dir": "./data/manually_collected",
    "patch_size": 32,
    "step_size": 10,
    "show_image": False,
    "save_image": True,
    "output_dir": "./data/output/pred_final4",
    # "in_video_file": "./data/manually_collected/video.mp4",
    # "out_video_name": "prediction_video.mp4",
    "score_threshold": 0.949
}
evaluate_config = {
    "validation_split": 0.10,
    "model_type": "VGG16",
    "model_path": "./models/mini_vgg_scratch_aug_b200_es_vs10/VGG16_24-0.95.model",
    "model_output_dir": "./models/{}".format(name_of_model_for_training),
    "epochs": 25,
    "saving_period": 2,
    "save_best_only": True,
    "log_dir_tensorboard": "./models/{}".format(name_of_model_for_training),
    "batch_size": 128,
    "early_stop_patience": 5,
    "augmentation": True,
    "optimiser": "adam"
}


def train(config, x_train, y_train, x_test, y_test):
    if config["model_type"] == "VGG16":
        ai_nn = VGG16(config)
    elif config["model_type"] == "MiniVGG16":
        ai_nn = MiniVGG(config)
    else:
        raise Exception("unable to train model type {}".format(config["model_type"]))

    ai_nn.fit(x_train, y_train)
    val_loss, val_acc = ai_nn.evaluate(x_test, y_test)
    logger.info("test_loss: {}, test_acc: {}".format(val_loss, val_acc))


def test(config, x_test, y_test):
    models_to_run = {
        "VGG16": [
            "vgg_16_scratch_aug_b128_es_vs10/VGG16_21-0.95.model",
            "vgg_image_net_aug_b128_es_vs10/VGG16_12-0.95.model",
            "vgg_scratch_aug_b10_es_vs10/VGG16_13-0.89.model",
            "vgg_scratch_aug_b30_es_vs10/VGG16_23-0.93.model",
            "vgg_scratch_aug_b50_es_vs10/VGG16_24-0.94.model",
            "vgg_scratch_aug_b70_es_vs10/VGG16_25-0.94.model",
            "vgg_scratch_aug_b150_es_vs10/VGG16_24-0.95.model"
        ],
        "MiniVGG16": ["mini_vgg_scratch_aug_b128_es_vs10/MiniVGG16_25-0.93.model"]
    }

    with open("./models/eval.txt", 'w') as f:
        f.write("Model Path,Validation Loss,Validation Acc,Elapsed Time\n")

    for model_type in models_to_run:
        for model_path in models_to_run[model_type]:
            config["model_path"] = "./models/{}".format(model_path)
            if model_type == "VGG16":
                ai_nn = VGG16(config)
            elif model_type == "MiniVGG16":
                ai_nn = MiniVGG(config)
            else:
                raise Exception("unable to train model type {}".format(config["model_type"]))
            import time
            start_time = time.time()
            ai_nn.load_model()
            val_loss, val_acc = ai_nn.evaluate(x_test, y_test)
            elapsed_time = time.time() - start_time
            output = "{},{},{},{}\n".format(model_path, val_loss, val_acc, elapsed_time)
            logger.info(output)
            with open("./models/eval.txt", "a") as f:
                f.write(output)


def run(config):
    ai_nn = VGG16(predict_config)
    ai_nn.load_model()

    ip = ImageProcessor(config)
    images = ip.read_images_from_dir()
    ip.run_on_all_images(images, ai_nn.predict)


def run_video(config):
    ai_nn = VGG16(predict_config)
    ai_nn.load_model()

    ip = ImageProcessor(config)
    ip.run_on_video(ai_nn.predict)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train and load models to find house numbers from images')
    parser.add_argument("-m", "--mode", dest="mode", required=True, help="train or predict")
    parser.add_argument("-lf", "--log_file", dest="log_file", default="logs", help="log directory, default logs")

    args = parser.parse_args()
    logger_util.initialise_logger(output_dir=args.log_file)
    if args.mode == "train":
        dh = DataHandlerSVHN(data_handler_config)
        x_train, y_train, x_test, y_test = dh.retrieve_train_test_data()
        train(training_config, x_train, y_train, x_test, y_test)
    if args.mode == "test":
        dh = DataHandlerSVHN(data_handler_config)
        _, _, x_test, y_test = dh.retrieve_train_test_data()
        test(evaluate_config, x_test, y_test)
    elif args.mode == "run":
        run(predict_config)
    elif args.mode == "run_video":
        run_video(predict_config)

# https://www.tensorflow.org/guide/keras
# https://www.tensorflow.org/api_docs/python/tf/keras/layers
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.concatenate.html
