

# cv_housenumbers

This is a project that simplifies the tasks of 
1. creating, training, evaluating neural networks 
2. reading SVHN dataset and preprocessing it 
3. using a trained model and an input image or video to search for house numbers

Note: Please refer to the following links for supplemental files

* [full project zip (~4gb onedrive)](https://gtvault-my.sharepoint.com/:u:/g/personal/tdhanani3_gatech_edu/EcQTnPBekNpFqIwsEBJJCYIBcqTW3A9-FxPZ7821eWPUQw?e=v0qkOL)  
* [VIDEO LINK (youtube)](https://youtu.be/3607w0Jt5FY)
* [REPORT (local reference)](./report/report.pdf)

Please email me if there is any confusion or questions: tdhanani3@gatech.edu
to download the complete code 

What was removed from this version to save space: 

  ```
  ├── data
│   ├── download
│   │   ├── test.tar.gz
│   │   ├── test_32x32.mat
│   │   ├── train.tar.gz
│   │   └── train_32x32.mat
│   ├── manually_collected
│   │   ├── IMG_20190416_184949.jpg
│   │   ├── IMG_20190420_154340.jpg
│   │   ├── IMG_20190421_154127.jpg
│   │   ├── IMG_20190421_160809.jpg
│   │   ├── IMG_20190421_161008.jpg
│   │   ├── IMG_20190421_172056.jpg
│   │   ├── IMG_20190421_172158.jpg
│   │   ├── IMG_20190424_223337.jpg
│   └── svhn_bg.zip
├── models
│   ├── image_net_vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5
│   ├── mini_vgg_scratch_aug_b128_es_vs10
│   │   ├── MiniVGG16_25-0.93.model
│   │   └── events.out.tfevents.1556169788.DGX-Boston
│   ├── vgg_16_scratch_aug_b128_es_vs10
│   │   ├── VGG16_21-0.95.model
│   │   └── events.out.tfevents.1556179559.DGX-Boston
│   ├── vgg_scratch_aug_b10_es_vs10
│   │   ├── VGG16_13-0.89.model
│   │   └── events.out.tfevents.1555885632.DGX-Boston
│   ├── vgg_scratch_aug_b150_es_vs10
│   │   ├── VGG16_24-0.95.model
│   │   └── events.out.tfevents.1555893329.DGX-Boston
│   ├── vgg_scratch_aug_b30_es_vs10
│   │   ├── VGG16_23-0.93.model
│   │   └── events.out.tfevents.1555888183.DGX-Boston
│   ├── vgg_scratch_aug_b50_es_vs10
│   │   ├── VGG16_24-0.94.model
│   │   └── events.out.tfevents.1555889803.DGX-Boston
│   └── vgg_scratch_aug_b70_es_vs10
│       ├── VGG16_25-0.94.model
│       └── events.out.tfevents.1555891352.DGX-Boston
  
  ```
  If any of these files are needed, please download the full project from [THIS LINK](https://gtvault-my.sharepoint.com/:u:/g/personal/tdhanani3_gatech_edu/EcQTnPBekNpFqIwsEBJJCYIBcqTW3A9-FxPZ7821eWPUQw?e=v0qkOL)

# Dataset 
Training various CNNs using tensorflow and this dataset:  http://ufldl.stanford.edu/housenumbers

## Getting Started

To run the code (as a TA) to generate the 5 images for validation of work use the following command: 

`python run.py`

note: the output of this command will be five images annotated and placed in the `./graded_images` directory. 

Other commands that can be played with are included below and should be self explanatory 

`python main.py -m get_data`

`python main.py -m train`

`python main.py -m run`

`python main.py -m run-video`

`python main.py -m test`

### Prerequisites

Highly recommended to use conda to install the dependencies.

```
conda env create -f cv_proj.yml
```

For this project, specifically relying on the following packages/libraries/versions:
- numpy=1.15.3
- python=3.6.7
- tensorflow-gpu=1.12.0
- cython=0.29
- opencv-python==3.4.3.18
