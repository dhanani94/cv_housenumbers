import logging
import os

from main import run
from utils import logger_util

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'  # not sure what this does
logger = logging.getLogger(__name__)

config = {
    "model_type": "VGG16",
    "model_path": "./models/vgg_image_net_aug_b128_es_vs10/VGG16_12-0.95.model",
    "target_dir": "./data/graded_input",
    "patch_size": 32,
    "step_size": 10,
    "show_image": False,
    "save_image": True,
    "output_dir": "./graded_images ",
    "score_threshold": 0.95
}

if __name__ == '__main__':
    logger_util.initialise_logger(output_dir="./logs")
    run(config)


